http://127.0.0.1:8000/rest-auth/user/ - to get all user info and to update user details
http://127.0.0.1:8000/rest-auth/logout/ - logout from any mode of login
http://127.0.0.1:8000/rest-auth/password/change/ - to change password when already login
http://127.0.0.1:8000/rest-auth/password/reset/ - to change reset password using email
http://127.0.0.1:8000/rest-auth/login/ - to login using email or username
